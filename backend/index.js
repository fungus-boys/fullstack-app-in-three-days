var express = require('express');
var bodyParser = require('body-parser');
var mongo = require('mongodb');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

var app = express();
app.listen(8080);

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization'); 
  next();
});

//root path
app.get('/', (req, res) => {
  res.send("Server root");
});

app.post('/user/', (req, res) => {
  if (typeof req.body.name === 'undefined' || typeof req.body.phone_number === 'undefined' || typeof req.body.user_type === 'undefined'){
    res.status(400).json({error: 'Bad request'});
    return;
  }

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var quizdb = db.db("quizdb");
    quizdb.createCollection("users", async function(err, collection){
      var user = await collection.findOne({
        phone_number: req.body.phone_number
      });
      if(user){
        res.status(400).json(user);
        return;
      }
      collection.insert({...req.body}, function(err, result){
        if(!err){
          console.log('Inserted: ', result);
          res.status(200).json(result.ops[0]);
          return;
        }
      });
    });
  });
});

app.post('/add-quiz/', (req,res) => {
  if (typeof req.body.quiz_name === 'undefined' || typeof req.body.questions === 'undefined'){
    res.status(400).json({error: 'Bad request'});
    return;
  }

  var questions = req.body.questions;

  for(let question of questions){
    if(typeof question.question === 'undefined' || typeof question.choices === 'undefined' || typeof question.answer === 'undefined'){
      res.status(400).json({error: 'Bad request'});
      return;
    }
  }

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var quizdb = db.db("quizdb");
    quizdb.createCollection("quizes", async function(err, collection){
      collection.insert({...req.body}, function(err, result){
        if(!err){
          console.log('Inserted: ', result);
          res.status(200).json(result.ops[0]);
          return;
        }
      });
    });
  });

});

app.get('/quiz/:quizId', (req, res) => {
  var quizId = req.params.quizId;

  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var quizdb = db.db("quizdb");
    quizdb.collection("quizes", async function(err, collection){

      try {
        var mongoQuizId = new mongo.ObjectID(quizId);
        collection.findOne({'_id': mongoQuizId}, function(err, result){
          if(!err){
            console.log('Found: ', result);
            if(result){
              res.status(200).json(result);
            } else {
              res.status(400).json({error: `Quiz with id ${quizId} not found!`});
            }
            return;
          }
        });
      } catch (error) {
        return res.status(400).json({error: `Quiz with id ${quizId} not found!`});
      }

    });
  });
});

app.get('/quizes/', (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var quizdb = db.db("quizdb");
    quizdb.collection("quizes").find({}).toArray(function(err, result){
      console.log(result);
      res.status(200).json(result);
    });
  });
});

app.post('/response/', (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var quizdb = db.db("quizdb");
    quizdb.createCollection("responses", function(err, collection) {
      if(!err){
        collection.insert({...req.body}, function(err, result) {
          if(!err){
            console.log('Inserted: ', result);
            let score = computeResult(result.ops[0].questions);
            res.status(200).json({score});
            return;
          }
        });
      }
    });
  });
})

function computeResult(questions) {

  let score = 0;

  for(let question of questions){

    if(question.answer == question.response) {
      score++;
    }

  }

  return score;

}

app.get('/user-types/', (req, res) => {
  res.status(200).json(['QUIZ_MASTER', 'PARTICIPANT']);
});