import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import QuizesList from "./pages/QuizesList";
import AddQuiz from "./pages/AddQuiz";
import QuizPreview from "./pages/QuizPreview";
import QuizForm from "./pages/Quiz";
import { appRoutes } from "./utils/config";

const Routes = () => {
    return (
        <BrowserRouter>
            <div>
                <Route path={appRoutes.quizes} exact component={QuizesList} />
                <Route path={appRoutes.addQuiz} exact component={AddQuiz} />
                <Route path={appRoutes.quizPreview} exact component={QuizPreview} />
                <Route path={appRoutes.quiz} exact component={QuizForm} />
            </div>
        </BrowserRouter>
    );
};

export default Routes;
