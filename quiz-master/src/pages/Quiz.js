import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { Card, Form, Button } from "react-bootstrap";
import { apis } from "../utils/apiConfig";
import { appRoutes } from "../utils/config";
import axios from 'axios';

const QuizForm = (props) => {
    // states
    const [questions, setQuestions] = useState({questions: []})
    const [userData, setUserData] = useState({ name: '', phone_number: '', user_type: 'QUIZ_MASTER' })
    const [isUserSignedUp, setUserSignedUp] = useState(false)

    // history
    const history = useHistory();

    // params
    const params = useParams();

    // effects
    useEffect(() => {
        const quizId = params.quizId
        axios
            .get(apis.quiz.replace(":quizId", quizId))
            .then((response) => {
                const tempData = response.data
                tempData.quiz_id = response.data._id
                delete tempData._id
                setQuestions(tempData)
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])

    // function - on user details change
    const onUserDetailsChange = (key, value) => {
        const tempUserData = { ...userData }
        tempUserData[key] = value
        setUserData(tempUserData)
    }

    // function - on user info
    const onUserInfo = (e) => {
        e.preventDefault();
        setUserSignedUp(true)
        axios
            .post(apis.user, userData)
            .then((response) => {
                setUserSignedUp(true)
                const tempQuestions = { ...questions }
                tempQuestions.user_id = response.data._id
                setQuestions(tempQuestions)
            })
            .catch((err) => console.log(err))
    }

    // function - onChange
    const onChange = (key, i) => {
        const tempQuestions = { ...questions }
        tempQuestions.questions[i].response = key
        setQuestions(tempQuestions)
    }

    // function - on submit
    const onSubmit = (e) => {
        e.preventDefault();
        axios
            .post(apis.submitQuiz, questions)
            .then((response) => {
                alert(`${response.data.score} out of ${questions.questions.length}`);
                history.push(appRoutes.quizes)
            })
            .catch((err) => console.log(err))
    }

    const formComponent = isUserSignedUp ? <Card className="m-4 p-5">
        <Card.Body>
            <Card.Title>Quiz</Card.Title>
            <Form onSubmit={onSubmit}>
                {questions.questions.length > 0 ?
                    <div>
                        {questions.questions.map((question, i) => {
                            return (
                                <div>
                                    <div>{question.question}</div>
                                    <fieldset>
                                        <Form.Group>
                                            {Object.entries(question.choices).map(([key, value]) => {
                                                return (
                                                    <Form.Check
                                                        type="radio"
                                                        label={value}
                                                        onChange={() => onChange(key, i)}
                                                        checked={key === question.response}
                                                    />
                                                )
                                            })}
                                        </Form.Group>
                                    </fieldset>
                                </div>
                            )
                        })}
                        <Button type="submit" className="float-right">Submit</Button>
                    </div> : <div>Quiz not found</div>}
            </Form>
        </Card.Body>
    </Card> : <Card className="m-4 p-5">
            <Form onSubmit={onUserInfo}>
                <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control required type="text" value={userData.name} placeholder="Name" onChange={(e) => onUserDetailsChange('name', e.target.value)} />
                </Form.Group>
                <Form.Group>
                    <Form.Label>Phone Number</Form.Label>
                    <Form.Control required type="number" value={userData.phoneNumber} placeholder="Phone Number" onChange={(e) => onUserDetailsChange('phone_number', e.target.value)} />
                </Form.Group>
                <Button type="submit" className="float-right">Submit</Button>
            </Form>
        </Card>
    return formComponent
}

export default QuizForm;