import React, { useState, useEffect } from "react";
import { Button, Card, Row, Col } from "react-bootstrap";
import { useHistory, useParams } from "react-router-dom";
import { appRoutes } from "../utils/config";
import { apis } from "../utils/apiConfig";
import axios from 'axios';

const QuizPreview = (props) => {
    // states
    const [quiz, setQuiz] = useState({questions: []})

    // history
    const history = useHistory();

    // params
    const params = useParams()

    // effects
    useEffect(() => {
        const quizId = params.quizId
        axios
            .get(apis.quiz.replace(":quizId", quizId))
            .then((response) => {
                setQuiz(response.data)
            })
            .catch((err) => {
                console.log(err)
            })
    }, [])

    // function - on done
    const onDone = () => {
        history.push(appRoutes.quizes)
    }
    return (
        <Card>
            <Card.Body>
                <Card.Title>Quiz Preview</Card.Title>
                {quiz.questions.length > 0 ?
                    <div>
                        {
                            quiz.questions.map((question) => {
                                return (
                                    <div className="mb-4">
                                        <div>{question.question}</div>
                                        <Row>
                                            {Object.values(question.choices).map((choice) => {
                                                return (<Col>{choice}</Col>)
                                            })}
                                        </Row>
                                    </div>
                                )
                            })
                        }
                        <Button onClick={onDone}>Done</Button>
                    </div> : <div>Quiz not found</div>}
            </Card.Body>
        </Card>
    )
}

export default QuizPreview;