import React, { useState } from "react";
import { Card, Form, Button, Row, Col } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { reverse } from "named-urls";
import { appRoutes } from "../utils/config";
import { apis } from "../utils/apiConfig";
import axios from 'axios';

const AddQuiz = (props) => {
    // constants
    const initialQuestion = { question: '', choices: { 1: '', 2: '', 3: '', 4: '' }, answer: 1 }
    const initialQuestions = { quiz_name: '', questions: [initialQuestion] }

    // states
    const [quiz, setQuiz] = useState(initialQuestions);

    // history
    const history = useHistory();

    // function - on question change
    const onQuestionChange = (e, i) => {
        const tempQuiz = { ...quiz };
        tempQuiz.questions[i].question = e.target.value;
        setQuiz(tempQuiz)
    };

    // function - on choice change
    const onChoiceChange = (e, parentIndex, key) => {
        const tempQuiz = { ...quiz };
        tempQuiz.questions[parentIndex].choices[key] = e.target.value;
        setQuiz(tempQuiz)
    }

    // function - on answer change
    const onAnswerChange = (e, i) => {
        const tempQuiz = { ...quiz };
        tempQuiz.questions[i].answer = e.target.value;
        setQuiz(tempQuiz)
    }

    // function - on answer change
    const onNameChange = (e) => {
        const tempQuiz = { ...quiz };
        tempQuiz.quiz_name = e.target.value;
        setQuiz(tempQuiz)
    }

    // function - add question
    const addQuestion = (e) => {
        e.preventDefault();
        const tempQuiz = { ...quiz };
        tempQuiz.questions.push(initialQuestion)
        setQuiz(tempQuiz)
    }

    // function - on submit
    const onSubmit = (e) => {
        e.preventDefault()
        axios
            .post(apis.addQuiz, quiz)
            .then((response) => {
                history.push(reverse(appRoutes.quizPreview, { quizId: response.data._id }))
            })
            .catch((err) => {
                console.log(err)
            })
    };


    return (
        <Card className="m-5">
            <Card.Body>
                <Card.Title className="d-flex justify-content-between">
                    <div>Create Quiz</div>
                    <Button size='sm' onClick={addQuestion}>Add Question</Button>
                </Card.Title>
                <Form onSubmit={onSubmit}>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control required type="text" value={quiz.name} placeholder="Name" onChange={(e) => onNameChange(e)} />
                    </Form.Group>
                    {quiz.questions.map((question, i) => {
                        return (
                            <Card className="mb-3">
                                <Card.Body>
                                    <Form.Group>
                                        <Form.Label>Question - {i + 1}</Form.Label>
                                        <Form.Control required type="text" value={question.question} placeholder="Question" onChange={(e) => onQuestionChange(e, i)} />
                                    </Form.Group>
                                    <Row>
                                        {Object.entries(question.choices).map(([key, choice]) => {
                                            return (
                                                <Col>
                                                    <Form.Group>
                                                        <Form.Label>Choice-{key}</Form.Label>
                                                        <Form.Control required type="text" value={choice} placeholder={`choice-${key}`} onChange={(e) => onChoiceChange(e, i, key)} />
                                                    </Form.Group>
                                                </Col>
                                            );
                                        })}
                                    </Row>
                                    <Form.Group>
                                        <Form.Label>Answer</Form.Label>
                                        <Form.Control as="select" value={question.answer} onChange={(e) => onAnswerChange(e, i)}>
                                            {Object.entries(question.choices).map(([key, choice]) => {
                                                return (
                                                    <option value={key}>{choice}</option>
                                                );
                                            })}
                                        </Form.Control>
                                    </Form.Group>
                                </Card.Body>
                            </Card>
                        );
                    })}
                    <Button className="float-right" type="submit">
                        Submit
                </Button>
                </Form>
            </Card.Body>
        </Card>
    )
}

export default AddQuiz