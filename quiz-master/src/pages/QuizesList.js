import React, { useEffect, useState } from 'react';
import { useHistory } from "react-router-dom";
import { reverse } from "named-urls";
import { apis } from '../utils/apiConfig';
import { Card } from 'react-bootstrap';
import { appRoutes } from '../utils/config';
import axios from 'axios';

const QuizesList = () => {
    // states
    const [quizes, setQuizes] = useState([])

    // history
    const history = useHistory()

    // effects
    useEffect(() => {
        axios
            .get(apis.quizes)
            .then((response) => {
                setQuizes(response.data)
            })
            .catch((err) => console.log(err))
    }, [])

    // function - open quiz
    const openQuiz = (quizId) => {
        history.push(reverse(appRoutes.quiz, { quizId }))
    }

    return (
        <div className="m-3">
            <h1>Quizes</h1>
            {quizes.map((quiz) => {
                return (
                    <div>
                        <Card onClick={() => openQuiz(quiz._id)}>
                            <Card.Body>
                                <Card.Title>{quiz.quiz_name}</Card.Title>
                            </Card.Body>
                        </Card>
                    </div>
                )
            })}
        </div>
    )
}

export default QuizesList;