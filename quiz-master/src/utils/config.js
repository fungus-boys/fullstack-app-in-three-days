const appRoutes = {

    // Quizes list
    quizes: "/",

    // Add Quiz
    addQuiz: "/add-quiz",

    // Quiz Preview
    quizPreview: "/preview/:quizId",

    // Quiz
    quiz: "/quiz/:quizId"
};

export { appRoutes };
