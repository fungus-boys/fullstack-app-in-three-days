const baseUrl = "http://localhost:8080"

const apis = {
    quizes: `${baseUrl}/quizes/`,
    quiz: `${baseUrl}/quiz/:quizId/`,
    addQuiz: `${baseUrl}/add-quiz/`,
    submitQuiz: `${baseUrl}/response/`,
    user: `${baseUrl}/user/`
}

export {apis}